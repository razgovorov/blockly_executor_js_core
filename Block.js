import {DeferredOperation, ErrorInBlock, ServiceException, StepForward} from "@/BlocklyExecutor/Core/exceptions"
import {isObject, objHasOwnProperty, uuid4} from "@/Helpers/BaseHelper"
import ExtException from "@/Helpers/ExtException"
import Logger from "@/BlocklyExecutor/Core/Logger"

export default class Block {
    ns = {'b': 'https://developers.google.com/blockly/xml'}
    _result = '_result'
    statement_inputs

    constructor(executor, {name, node, logger} = {}) {
        this.executor = executor
        this.workspace = executor.workspace
        this.name = name
        this.node = node
        // console.log(name, logger)

        this.logger = logger || new Logger()
        this.block_id = undefined
    }

    _check_step(context, block_context) {
        if (context.debug_mode) {
            if (context.is_next_step) {
                if (context.debug_mode === 'step') {
                    throw new StepForward([this.block_id, context, block_context, this.executor.workspace.name])
                } else {
                    context.current_block = this.block_id
                    context.current_workspace = this.executor.workspace.name
                }
            }
            if (this.block_id === context.current_block && context.current_workspace === this.executor.workspace.name) {
                if (this.executor.current_algorithm_breakpoints) {
                    if (this.block_id in this.executor.current_algorithm_breakpoints && context.is_next_step) {
                        throw new StepForward([this.block_id, context, block_context, this.executor.workspace.name])
                    }
                    if (context.debug_mode === 'step') {
                        if (context.is_next_step === false) {  // встаем на этом шаге
                            throw new StepForward([this.block_id, context, block_context, this.executor.workspace.name])
                        } else {
                            context.is_next_step = true  // встаем на следующем шаге
                        }
                    }
                }
            }
        }
    }

    async _before_execute(node, path, context, block_context) {
        this.block_id = this.workspace.get_block_id(node)
        this.block_type = this.workspace.get_block_type(node)
        block_context['__id'] = this.block_id
        block_context['__path'] = `${path}.${this.block_type}`
    }

    async execute_all_next(node, path, context, block_context, statement = false) {
        let statement_number = 0
        let child_context = context.get_child_context(block_context)
        let result
        for (; ;) {
            try {
                let _child_context
                if (statement) {
                    if (!objHasOwnProperty(child_context, String(statement_number))) {
                        child_context[String(statement_number)] = {}
                    }
                    _child_context = child_context[String(statement_number)]
                } else {
                    _child_context = child_context
                }

                let child = this.executor.workspace.read_child_block(node)

                let next_node

                if (child !== undefined) {
                    next_node = statement ? this.workspace.find_next_statement(child) : undefined
                    if (!objHasOwnProperty(_child_context, '__result')) {
                        result = await this.execute_child_block(child, block_context['__path'], context, _child_context)
                    } else {
                        this.logger.debug(`${this.workspace.get_block_id(child)} skip`)
                    }
                } else {
                    result = undefined
                    // raise Exception(f'{this.__class__.__name__} не хватает блока. path: {path} ')
                }
                if (next_node) {
                    node = next_node
                    // помечаем блок который выполнили
                    _child_context['__result'] = true
                    statement_number += 1
                } else {
                    context.clear_child_context(block_context)
                    return result
                }
            } catch (err) {
                throw err
            }
        }
    }

    async _execute(node, path, context, block_context) {
        // eslint-disable-next-line no-useless-catch
        try {
            return await this.execute_all_next(node, path, context, block_context)
        } catch (err) {
            throw err
        }
    }

    async execute_child_block(child_node, path, context, child_block_context) {
        let block_type = this.workspace.get_block_type(child_node)
        let _class = this.executor.get_block_class(block_type)
        try {
            let _class2 = new _class(this.executor, {logger: this.logger})
            return await _class2.execute(child_node, path, context, child_block_context)
        } catch (err) {
            if (err instanceof ServiceException || err instanceof ErrorInBlock || err instanceof DeferredOperation) {
                throw err
            }
            if (err instanceof ExtException) {
                throw err
            }
            if (err instanceof Error) {
                throw new ExtException({
                    message: 'Ошибка в блоке',
                    detail: `${block_type}: ${err}`,
                    parent: err,
                    dump: {
                        block: this.block_id,
                        algorithm: this.executor.workspace.name
                    }
                })
            }
        }
    }

    async execute(node, path, context, block_context) {
        try {
            await this._before_execute(node, path, context, block_context)
            this.logger.debug(`begin execute ${this.block_type} id="${this.block_id}"`)
            return await this._execute(node, path, context, block_context)
        } catch (err) {
            throw err
        }
    }

    get_variable(context, name) {
        if (objHasOwnProperty(context.block_context, '__thread_vars') && objHasOwnProperty(context.block_context['__thread_vars'], name)) {
            return context.block_context['__thread_vars'][name]
        }
        if (objHasOwnProperty(context.variables, name)) {
            return context.variables[name]
        }
        throw new ExtException({message:'Variable not defined', detail: name})
    }

    set_variable(context, name, value) {
        if (this.executor.multi_thread_mode) {
            if (!context.block_context['__thread_vars']) {
                context.block_context['__thread_vars'] = {}
            }
            context.block_context['__thread_vars'][name] = value
        }
        context.variables[name] = value
    }

    commandSend(command_name, command_params, context, block_context) {
        const commandUuid = uuid4()
        block_context['__deferred'] = commandUuid
        context.commands.push(
            {
                'name': command_name,
                'params': command_params,
                'uuid': commandUuid
            }
        )
        throw new DeferredOperation(commandUuid, context, block_context)
    }

    static get fullName() {
        return this.name
    }

    commandGetResult(commandUuid) {
        let commandsResultData
        let _status
        let _data
        try {
            commandsResultData = this.executor.commands_result[commandUuid]
            if (!commandsResultData) {
                throw new Error()
            }
        } catch (e) {
            throw new ExtException({
                message: 'Command no response',
                detail: `block ${Block.fullName} command_uuid ${commandUuid}`
            })
        }
        try {
            _status = commandsResultData['status']
            _data = commandsResultData['data']
        } catch (e) {
            throw new ExtException({
                message: 'Bad format command result',
                detail: `block ${Block.fullName} command_uuid ${commandUuid}`,
                dump: {'result': commandsResultData}
            })
        }
        if (_status.toUpperCase() === 'COMPLETE') {
            return _data
        }
        if (_status.toUpperCase() === 'ERROR') {
            if (isObject(_data)) {
                throw new ExtException({
                    parent: _data
                })
            }
            throw new Error(_data)
        }
        throw new ExtException({
            message: 'Not supported command result type',
            detail: `block ${Block.fullName} result type ${_status}`,
        })
    }

    static commandSended(block_context) {
        return '__deferred' in block_context
    }

    _get_mutation(node, mutationName, defaultValue) {
        const mutationValue = this.workspace.get_mutation(node, mutationName)
        return mutationValue || defaultValue
    }
}
