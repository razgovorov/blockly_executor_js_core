/* eslint-disable no-unused-vars */
import ExtException from "@/Helpers/ExtException"
import ContextProps from "@/BlocklyExecutor/Core/ContextProps"
import {WorkspaceNotFound, LimitCommand} from "@/BlocklyExecutor/Core/exceptions"
import {objHasOwnProperty, uuid4} from "@/Helpers/BaseHelper"
import {findByKey} from "@/Helpers/ArrayHelper";
import Helper from "@/Helpers/Helper";

export default class Context extends ContextProps {
    constructor() {
        super()
        this.uid = undefined
        this.data = {}
        this.action = undefined //Action()
        this.current_thread = undefined

        this.is_deferred = false
        this.is_next_step = false
        this.deferred_result = undefined
        this.limit_commands = 25
    }

    async init({uid, debug_mode, current_block, current_workspace, workspace_name, data, service} = {}) {
        this.uid = uid ? uid : uuid4()
        this.data = data || {}
        this.debug_mode = debug_mode
        this.current_block = current_block
        this.current_workspace = current_workspace || workspace_name
        this.workspace_name = workspace_name
        this.is_next_step = !current_block && this.debug_mode ? true : undefined
        this.service = service
    }

    init_deferred(block_context) {
        const _class = new Context()
        _class.block_context = block_context['block_context']
        _class.is_deferred = true
        _class.variables = this.variables
        _class.deferred = this.deferred
        return _class
    }

    init_nested(block_context, workspace_name) {
        const _class = new Context()
        _class.data = block_context._child || {}
        _class.workspace_name = workspace_name
        _class.workspace_file_type = this.workspace_file_type

        _class.is_deferred = this.is_deferred
        _class.debug_mode = this.debug_mode
        _class.current_block = this.current_block
        _class.current_workspace = this.current_workspace
        _class.is_next_step = this.is_next_step
        return _class
    }

    to_parallel_dict(self) {
        // return dict(
        //     variables = this.variables,
        //     block_context = this.block_context,
        // )
    }

    to_dict(self) {
        // return this.data
    }

    to_result(self) {
        let res = {
            uid: this.uid,
            result: this.result,
            status: this.status,
            commands: this.commands,
            raw: this.data
        }
        if (this.debug_mode) {
            res['current_variables'] = this.current_variables
            res['current_block'] = this.current_block
            res['current_workspace'] = this.current_workspace
        }
        return res
    }

    set_next_step(block_id) {
        if (this.debug_mode) {
            if (this.debug_mode === 'step') {
                if (block_id || (block_id === this.current_block && this.current_workspace === this.workspace_name)) {
                    this.is_next_step = true
                }
            } else {
                this.is_next_step = false
            }
        }
    }

    set_step(block_id) {
        if (this.debug_mode) {
            // # if this.executor.current_block != this.block_id:
            this.is_next_step = false
            this.current_block = block_id
            this.current_workspace = this.workspace_name
        }
    }

    get_child_context(block_context) {
        if (!objHasOwnProperty(block_context, '__child')) {
            block_context['__child'] = {}
        }
        return block_context['__child']
    }

    clear_child_context(block_context, result, delete_children = true) {
        if (delete_children) {
            delete block_context['__child']
        }
    }

    copy() {
        const _this = new Context()
        _this.deferred = this.deferred
        _this.block_context = Helper.copyViaJson(this.block_context)
        return _this
    }

    add_deferred(deferred_exception) {
        //
        const _localContext = deferred_exception.block_context
        const _operationContext = deferred_exception.context

        let i
        try {
            i = findByKey(this.deferred, _localContext.__deferred, '__deferred')
            if (i === -1) {
                throw new Error()
            }
        } catch (err) {
            this.deferred.push({})
            i = this.deferred.length - 1
        }

        this.deferred[i] = {
            __deferred: _localContext.__deferred,
            block_context: Helper.copyViaJson(_operationContext.block_context)
        }

        // try {
        //     i = ArrayHelper.findByKey(this.commands, _localContext.__path, 2)
        //     if (i === -1) {
        //         throw new Error()
        //     }
        // } catch (err) {
        //     this.commands.push([])
        //     i = this.commands.length - 1
        // }
        // this.commands[i] = deferred_exception.toCommand()
    }

    check_command_limit() {
        if (this.commands.length >= this.limit_commands) {
            throw new LimitCommand()
        }
    }

    setCommandLimit(debug_mode) {
        this.limit_commands = debug_mode ? 1 : 25
    }

    variable_scope_add(block_context, params) {
        if (objHasOwnProperty(block_context, '_variable_scope')) {
            this.current_variable_scope = block_context['_variable_scope']
        } else {
            this.variable_scopes.push(params)
            this.current_variable_scope = this.variable_scopes.length - 1
            block_context['_variable_scope'] = this.current_variable_scope
        }
    }

    variable_scope_remove(block_context) {
        if (block_context['_variable_scope'] !== this.current_variable_scope || this.variable_scopes.length - 1 !== this.current_variable_scope) {
            throw new ExtException({message: 'Какая то фигня с variable_scope'})
        }
        this.variable_scopes.pop(this.current_variable_scope)
        this.current_variable_scope -= 1
    }

    async workspaceRead() {
        if (!objHasOwnProperty(this.service.workspace_cache, this.workspace_name)) {
            throw new WorkspaceNotFound(this.workspace_name)
        }
        return this.service.workspace_cache[this.workspace_name]
    }
}
