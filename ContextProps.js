export default class ContextProps {
    constructor() {
        this.data = {}
        this.current_variable_scope = 0
    }

    get variables() {
        return this.variable_scopes[this.current_variable_scope]
    }

    set variables(value) {
        this.variable_scopes[this.current_variable_scope] = value
    }

    get variable_scopes() {
        return this.data_getter_root('variable_scopes', [{}])
    }

    set variable_scopes(value) {
        this.data_setter_root('variable_scopes', value)
    }

    get block_context() {
        return this.data_getter_root('block_context', {})
    }

    set block_context(value) {
        this.data_setter_root('block_context', value)
    }

    get current_workspace() {
        return this.data_getter_root('current_workspace', '')
    }

    set current_workspace(value) {
        this.data_setter_root('current_workspace', value)
    }

    get current_variables() {
        return this.data_getter_root('current_variables', {})
    }

    set current_variables(value) {
        this.data_setter_root('current_variables', value)
    }

    get deferred() {
        return this.data_getter_root('deferred', [])
    }

    set deferred(value) {
        this.data_setter_root('deferred', value)
    }

    get status() {
        return this.data_getter_root('status', '')
    }

    set status(value) {
        this.data_setter_root('status', value)
    }

    get result() {
        return this.data_getter_root('result', {})
    }

    set result(value) {
        this.data_setter_root('result', value)
    }

    get commands() {
        return this.data_getter_root('commands', {})
    }

    set commands(value) {
        this.data_setter_root('commands', value)
    }

    get workspace_name() {
        return this.data_getter_root('workspace_name', '') // 'algorithm'
    }

    set workspace_name(value) {
        this.data_setter_root('workspace_name', value)
    }

    get current_block() {
        return this.data_getter_root('current_block')
    }

    set current_block(value) {
        this.data_setter_root('current_block', value)
    }

    get debug_mode() {
        return this.data_getter_root('debug_mode')
    }

    set debug_mode(value) {
        this.data_setter_root('debug_mode', value)
    }

    data_getter_root(name, default_value) {
        if (Object.prototype.hasOwnProperty.call(this.data, name)) {
            return this.data[name]
        }
        this.data[name] = default_value
        return this.data[name]
    }

    data_setter_root(name, value) {
        this.data[name] = value
    }
}
