/* eslint-disable no-unused-vars */
import ExtException from "@/Helpers/ExtException"
import WorkspaceJson from "@/BlocklyExecutor/Core/WorkspaceJson"
import Logger from "@/BlocklyExecutor/Core/Logger"
import {isObject, getJsonType} from "@/Helpers/BaseHelper"
import WorkspaceXml from "@/BlocklyExecutor/Core/WorkspaceXml";
import {DeferredOperation} from "@/BlocklyExecutor/Core/exceptions";

export default class BlocklyExecutor {
    _blocks_index

    constructor({logger, plugins, breakpoints} = {}) {
        this.breakpoints = breakpoints
        this.logger = logger || new Logger()
        this.plugins = plugins
        this.multi_thread_mode = false

        this.gather = []

        this.commands_result = {}
        this.workspace = undefined
    }

    async execute({context, endpoint, commands_result} = {}) {
        try {
            const workspace_raw = await context.workspaceRead()
            this.workspace = this.initWorkspace(workspace_raw, context.workspace_name, this.logger)
            context.status = 'run'
            context.setCommandLimit(context.debug_mode)
            const startBlock = this.workspace.get_start_block(this, endpoint, context)
            this._indexCommandsResult(context, commands_result)
            context.commands = []

            this.logger.debug('--------execute----------------------------')

            if (context.deferred.length) {
                await this._executeDeferred(startBlock, context)
                context.current_variable_scope = 0
            }
            context.check_command_limit()

            context.result = await startBlock.execute(startBlock.node, '', context, context.block_context)
            this.logger.debug('Complete')
            context.status = 'complete'
        } catch (err) {
            switch (err.constructor.name) {
                case 'DeferredOperation':
                case 'LimitCommand':
                    this.logger.debug(`raise ${err.constructor.name}`)
                    break
                case 'WorkspaceNotFound':
                    context.status = 'error'
                    context.result = {__name__: err.constructor.name, detail: err.message}
                    break
                case 'StepForward':
                    context.result = err.args[2]  // block_context
                    context.current_variables = err.args[1].variables
                    context.current_block = err.args[0]
                    context.current_workspace = err.args[3]
                    this.logger.debug('raise StepForward')
                    break
                case 'ExtException':
                    context.status = 'error'
                    context.result = err.toDict()
                    break
                default:
                    context.status = 'error'
                    context.result = new ExtException({parent: err}).toDict()
            }
        }
        return context
    }

    async execute_nested(context, endpoint, commands_result) {
        const workspace_raw = await context.workspaceRead()
        this.workspace = this.init_workspace(workspace_raw, context.workspace_name, this.logger)
        const startBlock = this.workspace.get_start_block(endpoint, context)
        return await startBlock.execute(startBlock.node, '', context, context.block_context)
    }

    init_workspace(workspace_raw, algorithm, logger) {
        if (!workspace_raw) {
            throw new ExtException({
                message: 'Передан пустой алгоритм',
                detail: algorithm
            })
        }

        if (isObject(workspace_raw)) {
            return new WorkspaceJson(workspace_raw, algorithm, logger)
        }
        if (typeof workspace_raw === 'string') {
            return new WorkspaceXml(workspace_raw, algorithm, logger)
        }
    }

    get blocks_index() {
        if (this._blocks_index) {
            return this._blocks_index
        }
        this._blocks_index = {}
        if (this.plugins) {
            this.plugins.forEach(plugin => {
                for (let blockId in plugin.blocks) {
                    this._blocks_index[blockId] = plugin.blocks[blockId]
                }
            })
        }
        return this._blocks_index
    }

    get_block_class(block_name) {
        let block_class = this.blocks_index[block_name]
        if (block_class) {
            return block_class
        }
        throw new ExtException({message: 'Block handler not found', detail: block_name})
    }

    async _executeDeferred(robot, context) {
        this.logger.debug('')
        this.logger.debug('--------execute deferred--------------')
        const _deferred = context.deferred
        const _commands = context.commands
        context.commands = []
        const _delete = []

        for (let i = 0; i < _deferred.length; i++) {
            const _context = context.init_deferred(context, _deferred[i])
            try {
                await robot.execute(robot.node, '', _context, _context.block_context)
                _delete.push(i)
            } catch (err) {
                if (err instanceof DeferredOperation) {
                    context.add_deferred(err)
                    _delete.push(i)
                }
            }
        }
        _delete.reverse()
        for (const elem in _delete) {
            context.deferred.pop(elem)
        }
    }

    current_algorithm_breakpoints() {
        try {
            return this.breakpoints[this.workspace.name]
        } catch (e) {
            return null
        }
    }

    initWorkspace(workspace_raw, algorithm, logger) {
        if (!workspace_raw) {
            throw new ExtException({message: 'Передан пустой алгоритм', detail: algorithm})
        }
        let workspace_type = getJsonType(workspace_raw)
        switch (workspace_type) {
            case 'string':
                if (workspace_raw[0] === '{') {
                    return new WorkspaceJson(workspace_raw, algorithm, logger)
                }
                return new WorkspaceXml(workspace_raw, algorithm, logger)
            case 'object':
                return new WorkspaceJson(workspace_raw, algorithm, logger)
            default:
                throw new ExtException({message: 'Неподдерживаемый формат файла', detail: algorithm})
        }

    }

    _indexCommandsResult(context, commandsResult) {
        if (Array.isArray(commandsResult)) {
            for (const command of commandsResult) {
                const commandUuid = command.uuid
                if (commandUuid) {
                    this.commands_result[commandUuid] = command
                    continue
                }
                throw new ExtException({
                    message: 'У результата команды не указан идентификатор'
                })
            }
        }
        for (const commandKey in context.commands) {
            const commandUuid = context.commands[commandKey].uuid
            if (commandUuid && !this.commands_result[commandUuid]) {
                throw new ExtException({
                    message: 'Не получен ответ на команду',
                    detail: commandUuid
                })
            }
        }
    }
}
