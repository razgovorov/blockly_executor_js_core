export const CRITICAL = 50

export const ERROR = 40

export const WARNING = 30

export const INFO = 20

export const DEBUG = 10

export const NOTSET = 0

export default class Logger {

    constructor(mode = 10) {
        this.mode = Number(mode)
    }
    critical(msg) {
        if (this.mode >= CRITICAL){
            console.error(msg)
        }
    }


    warning(msg) {
        if (this.mode >= WARNING){
            console.warn(msg)
        }
    }

    error(msg) {
        if (this.mode >= ERROR){
            console.error(msg)
        }
    }

    info(msg) {
        if (this.mode >= INFO){
            console.log(msg)
        }
    }

    debug(msg) {
        if (this.mode >= DEBUG){
            console.log(msg)
        }
    }
}