import Root from "@/BlocklyExecutor/Core/blocks/root"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"

export default class Workspace {
    constructor(data, name, logger) {
        this.name = name
        this.data = data
        this.blocks = undefined
        this.functions = undefined
        this.variables = undefined
        this.read_variables()
        this.logger = logger
    }

    get_start_block(executor, endpoint, context) {
        this.read_procedures_and_functions()
        let block
        if (endpoint) {
            if (!objHasOwnProperty(this.functions, endpoint)) {
                context.status = 'error'
                context.result = `not found endpoint ${endpoint}`
                return context
            }
            block = this.init_procedure_block(executor, endpoint)
        } else {
            if (objHasOwnProperty(this.functions, 'main')) {
                block = this.init_procedure_block(executor, 'main')
            } else {
                block = new Root(executor,  {node: this.blocks, logger: this.logger})
            }
        }
        return block
    }

    // eslint-disable-next-line no-unused-vars
    init_procedure_block(executor, endpoint) {
        throw new Error('Not implemented')
    }
    read_procedures_and_functions(){
        throw new Error('Not implemented')
    }
    read_variables(){
        throw new Error('Not implemented')
    }
}