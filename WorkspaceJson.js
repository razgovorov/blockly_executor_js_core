import Workspace from "@/BlocklyExecutor/Core/Workspace"
import {getType, objHasOwnProperty} from "@/Helpers/BaseHelper"
import Procedures_defnoreturn from "@/BlocklyExecutor/Core/blocks/procedures_defnoreturn"
import Procedures_defreturn from "@/BlocklyExecutor/Core/blocks/procedures_defreturn"

export default class WorkspaceJson extends Workspace {
    constructor(data, name, logger) {
        super(data, name, logger)
        try {
            this.blocks = data['blocks']['blocks']
        } catch (err) {
            this.blocks = []
        }
        this.variables = data['variables'] || []
    }
    get_block_type(node) {
        return node.type
    }
    get_block_id(node) {
        return node.id
    }
    read_procedures_and_functions() {
        this.functions = {}
        const handlers = {
            'procedures_defreturn': Procedures_defreturn,
            'procedures_defnoreturn': Procedures_defnoreturn
        }

        this.blocks.forEach(node => {
            if (objHasOwnProperty(handlers, node['type'])) {
                let name = node['fields']['NAME']
                this.functions[name] = [handlers[node['type']], node]
            }
        })
    }

    init_procedure_block(executor, name) {
        let block_class
        let node
        [block_class, node] = this.functions[name]
        return new block_class(executor, {node, logger: this.logger})
    }

    read_variables() {
        this.variables = {}
        let variables = this.data['variables'] || []
        variables.forEach(variable => {
            this.variables[variable['id']] = variable['name']
        })
    }

    // @classmethod
    read_child_block(node) {
        let child
        if (node) {
            switch (getType(node)) {
                case 'object':
                    child = node['block']
                    if (child === undefined) {
                        return node['shadow']
                    }
                    break
                case 'array':
                    for (let i = 0; i < node.length; ++i) {
                        if (['procedures_defreturn', 'procedures_defnoreturn'].indexOf(node[i]['type']) < 0) {
                            return node[i]
                        }
                    }
                    break
                default:
                    throw new Error('Not implemented')
            }
        }
        return child
    }

    find_child_blocks(node) {
        let standard_blocks = []
        let child
        if (node) {
            switch (getType(node)) {
                case 'object':
                    child = node['block']
                    if (child === undefined) {
                        child = node['shadow']
                    }
                    if (child !== undefined) {
                        standard_blocks.push(child)
                    }
                    break
                case 'array':
                    standard_blocks = node
                    break
            }
        }
        return standard_blocks
    }

    find_field_by_name(node, name) {
        try {
            return node['fields'][name]
        } catch (err) {
            return undefined
        }
    }

    //
    // @classmethod
    find_statement_by_name(node, name) {
        return this.find_input_by_name(node, name)
    }

    //
    // @classmethod
    find_next_statement(node) {
        try {
            return node['next']
        } catch (err) {
            return undefined
        }
    }

    find_mutation_by_name(node, name, default_value) {
        let mutation = node.get('extraState')
        if (mutation !== undefined) {
            return mutation['name'] || default_value
        }
        return default_value
    }

    find_mutation_args(node) {
        let mutation = node['extraState']
        if (mutation !== undefined) {
            return mutation['params']
        }
        return undefined
    }

    find_inputs(node) {
        return node['inputs'] || {}
    }

    find_input_by_name(node, name) {
        try {
            return node['inputs'][name]
        } catch (err) {
            return undefined
        }
    }

    find_fields(block, node, path, context, block_context) {
        let fields = node['fields']
        if (fields !== undefined) {
            for (let _param_name in fields) {
                if (objHasOwnProperty(fields, _param_name)) {
                    let _value = fields[_param_name]
                    switch (getType(_value)) {
                        case 'object':
                            block_context[_param_name] = this.variables[_value['id']]
                            break
                        default:
                            block_context[_param_name] = _value
                    }

                }
            }
        }
    }

    async execute_inputs(block, node, path, context, block_context) {
        try {
            let inputs = node['inputs']
            if (inputs !== undefined) {
                for (let name in inputs) {
                    if (!objHasOwnProperty(inputs, name)) {
                        continue
                    }
                    if (block.statement_inputs && block.statement_inputs.indexOf(name) >= 0) {
                        continue
                    }
                    // this.logger.debug(`execute inputs ${node.type} ${name}`)
                    if (!objHasOwnProperty(block_context, name)) {
                        try {
                            block_context[name] = await block.execute_all_next(
                                inputs[name], `${path}.${name}`, context, block_context
                            )
                        } catch (err) {
                            throw err
                        }
                    }
                }
            }
        } catch
            (err) {
            throw err
        }
    }

    get_mutation(node, mutationName) {
        const mutation = node.mutation
        if (mutation) {
            return mutation[mutationName]
        }
    }
}
