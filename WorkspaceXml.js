import Procedures_defnoreturn from "@/BlocklyExecutor/Core/blocks/procedures_defnoreturn";
import Procedures_defreturn from "@/BlocklyExecutor/Core/blocks/procedures_defreturn";
import Workspace from "@/BlocklyExecutor/Core/Workspace";

export default class WorkspaceXml extends Workspace {
    constructor(data, name, logger) {
        data = WorkspaceXml.workspaceToTree(data)
        super(data, name, logger)
        this.blocks = this.data
    }

    static workspaceToTree(workspaceRaw) {
        const XmlTree = new window.DOMParser();
        return XmlTree.parseFromString(workspaceRaw, 'application/xml').children[0]
    }

    read_procedures_and_functions() {
        this.functions = {}
        const handlers = {
            'procedures_defreturn': Procedures_defreturn,
            'procedures_defnoreturn': Procedures_defnoreturn
        }
        this.data.querySelectorAll('block[type=procedures_defreturn]')
            .forEach((node) => {
                const name = node.querySelector('field[name=NAME]').textContent;
                this.functions[name] = [handlers[node.getAttribute('type')], node];
            });

        this.data.querySelectorAll('block[type=procedures_defnoreturn]')
            .forEach((node) => {
                const name = node.querySelector('field[name=NAME]').textContent;
                this.functions[name] = [handlers[node.getAttribute('type')], node];
            });
    }

    init_procedure_block(executor, name) {
        const [blockClass, node] = this.functions[name]
        return new blockClass(executor, {node, logger: this.logger})
    }

    find_input_by_name(node, name) {
        try {
            return node.querySelector(`:scope > value[name='${name}']`)
        } catch (err) {
            return undefined
        }
    }

    find_field_by_name(node, name) {
        return node.querySelector(`:scope > field[name='${name}']`).textContent
    }

    /**
     * Ищит ноды block или shadow
     */
    read_child_block(node) {
        let child
        if (node) {
            child = node.querySelector('block')
            if (!child) {
                child = node.querySelector('shadow')
            }
        }
        return child
    }

    find_fields(node, result) {
        result ||= {}
        const fields = Array.from(node.children).filter(child => child.tagName.toLowerCase() === 'field')
        if (!fields) {
            return null
        }
        for (const field of fields) {
            const paramName = field.getAttribute('name')
            result[paramName] = field.textContent
        }
        return result
    }

    read_variables() {
        this.variables = {}
        for (const node of this.data.querySelectorAll('variables variable')) {
            const name = node.textContent
            const _id = node.getAttribute('id')
            this.variables[_id] = name
        }
        return this.variables
    }

    find_statement_by_name(node, name) {
        if (name) {
            return node.querySelector(`:scope > statement[name='${name}']`)
        }
        return node.querySelector(':scope > statement')
    }

    /**
     * Ищит ноду next
     * @param node
     * @return {any}
     */
    find_next_statement(node) {
        // const nodes = Array.from(node.children).filter(child => child.tagName.toLowerCase() === 'next')
        // if (nodes.length) {
        //     return nodes[0]
        // }
        // return null
        return node.querySelector(':scope > next')
    }

    async execute_inputs(block, node, path, context, block_context) {
        const inputs = Array.from(node.children).filter(child => child.tagName.toLowerCase() === 'value')
        if (!inputs) {
            return
        }

        for (const input of inputs) {
            const _paramName = input.getAttribute('name')
            if (!(_paramName in block_context)) {
                block_context[_paramName] = await block.execute_all_next(
                    input,
                    `${path}.${_paramName}`,
                    context,
                    block_context
                )
            }
        }
    }

    find_mutation_by_name(node, name, defaultValue) {
        const mutation = node.querySelector(':scope > mutation')
        if (!mutation) {
            return defaultValue
        }
        return mutation.getAttribute(name) || defaultValue
    }

    find_mutation_args(node) {
        const result = []
        const args = node.querySelectorAll(':scope > mutation arg')
        if (args) {
            for (const arg of args) {
                result.push(arg.getAttribute('name'))
            }
        }
        return result
    }

    find_inputs(node) {
        const result = {}
        for (const child of node.children) {
            if (['value', 'statement'].includes(child.tagName.toLowerCase())) {
                const inputName = child.getAttribute('name')
                result[inputName] = child
            }
        }
        return result
    }

    get_block_type(node) {
        return node.getAttribute('type')
    }

    get_block_id(node) {
        return node.getAttribute('id')
    }

    get_mutation(node, mutationName) {
        const mutation = node.querySelector('mutation')
        if (mutation) {
            return mutation.getAttribute(mutationName)
        }
    }
}
