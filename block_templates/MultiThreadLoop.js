import Block from "@/BlocklyExecutor/Core/Block";
import {DeferredOperation, LimitCommand} from "@/BlocklyExecutor/Core/exceptions";

export default class MultiThreadLoop extends Block {
    statement_inputs = ['STACK']

    constructor(executor, params) {
        super(executor, params);
        this.deferredOnlyOnePage = false
    }

    // eslint-disable-next-line no-unused-vars
    async _getItems(context, block_context) {
        throw new Error('_getItems NotImplemented')
    }

    async _executeItem(nodeLoop, path, context, block_context) {
        this.logger.debug(`${this.block_id} execute page ${block_context.page} item ${block_context.index}`)
        await this.execute_all_next(nodeLoop, path, context, block_context, true)
    }

    // eslint-disable-next-line no-unused-vars
    _onBeforeLoop(node, path, context, block_context) {}

    _onLoop(node, path, context, block_context) {
        this.set_variable(context, node.children[0].textContent, block_context.items[block_context.index])
    }

    // eslint-disable-next-line no-unused-vars
    _onDeferredItem(node, path, context, block_context, deferred) {}

    // eslint-disable-next-line no-unused-vars
    _calcItem(node, path, context, block_context) {}

    async _executeItems(node, path, context, block_context) {
        this._onBeforeLoop(node, path, context, block_context)
        let nodeLoop
        try {
            nodeLoop = this.workspace.find_statement_by_name(node)
        } catch (e) {
            return
        }
        if (!block_context.items) {
            block_context.items = []
        }
        // eslint-disable-next-line no-constant-condition
        while (true) {
            if (!block_context.items.length) {
                block_context.page = block_context.page === undefined ? 0 : block_context.page + 1

                block_context.items = await this._getItems(context, block_context)
                block_context.index = 0
                if (!block_context.items.length) {
                    if (context.commands.length) {
                        throw new LimitCommand()
                    }
                    return;
                }
            }
            for (let i = block_context.index; i < block_context.items.length; i++) {
                block_context.index = i
                try {
                    context.check_command_limit()
                } catch (err) {
                    if (err instanceof LimitCommand) {
                        context.clear_child_context(block_context)
                    }
                }
                this._onLoop(node, path, context, block_context)
                this._check_step(context, block_context)
                try {
                    await this._executeItem(nodeLoop, `${path}.${block_context.page}_${block_context.index}`, context, block_context)
                } catch (err) {
                    if (err instanceof DeferredOperation) {
                        if (!err.commandUuid) {
                            throw new Error('В цикле есть блоки параллельная работа которых не поддерживается')
                        }
                        context.add_deferred(err)
                        this._onDeferredItem(node, path, context, block_context, err)
                        context.set_next_step(this.block_id)
                    } else {
                        throw err
                    }
                }
                context.set_step(this.block_id)
                context.clear_child_context(block_context)
            }

            block_context.items = []

            try {
                context.check_command_limit()
            } catch (err) {
                throw err
            }

            if (this.deferredOnlyOnePage && context.commands.length) {
                throw new LimitCommand()
            }
        }
    }

    async _execute(node, path, context, block_context) {
        this.executor.multi_thread_mode = true
        if (context.is_deferred) {
            const i = block_context.index
            await this._executeItems(node, `${path}.${i}`, context, block_context)
            context.set_step(this.block_id)
            return
        }
        await this._executeItems(node, path, context, block_context)
        this.executor.multi_thread_mode = false
    }
}
