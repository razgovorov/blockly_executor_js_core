import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";
import Block from "@/BlocklyExecutor/Core/Block";

export default class ExtsysConnectorApi extends SimpleBlock {
    suffix = 'find'
    calc_params = ['object']

    async _calc_value(node, path, context, block_context) {
        if (Block.commandSended(block_context)) {
            return this.commandGetResult()
        }
        const iniName = `${block_context.ini_name}_${this.suffix}`
        await updateIni(this.executor, context, iniName)
        const params = {
            ini_name: iniName
        }
        for (const elem of this.calc_params) {
            params[elem] = block_context[elem]
        }
        return this.commandSend('calc_ini', params, context, block_context)
    }
}

export async function updateIni(executor, context, iniName) {
    let cache
    try {
        cache = context.data['cache_ini']
        if (!cache) {
            throw new TypeError()
        }
    } catch (e) {
        context.data['cache_ini'] = {}
        cache = context.data['cache_ini']
    }
    if (iniName in cache) {
        return
    }
    if (!context.report) {
        throw new Error('Report not defined')
    }
    context.commands.push({
        'name': 'update_ini',
        'params': {
            'name': iniName,
            'version': 0,
            'value': await context.report.service.workspaceRead(context.report.operation, iniName)
        }
    })
    cache[iniName] = 0
}
