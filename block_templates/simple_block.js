import Block from "@/BlocklyExecutor/Core/Block"
import {ServiceException} from "@/BlocklyExecutor/Core/exceptions"
import ExtException from "@/Helpers/ExtException"


export class SimpleBlock extends Block {
    required_param = []
    step = true

    async _execute(node, path, context, block_context) {
        try {
            if (!block_context[this._result]) {
                this.workspace.find_fields(node, block_context)
                await this.workspace.execute_inputs(this, node, path, context, block_context)
                if (this.step) {
                    this._check_step(context, block_context)
                }
                this.check_required_param_in_block_context(block_context)
                block_context[this._result] = await this._calc_value(node, path, context, block_context)
            } else {
                this.logger.debug('skip calc')
            }
            return block_context[this._result]
        } catch (err) {
            if (err instanceof ServiceException) {
                throw err
            }
            switch (err.constructor.name) {
                case 'ExtException':
                    throw err
                default:
                    throw new ExtException({parent: err, action: `${this.constructor.name}`})
            }
        }
    }

    check_required_param_in_block_context(block_context) {
        for (const paramName of this.required_param) {
            if (!(paramName in block_context)) {
                throw new ExtException({
                    message: "Required param not defined",
                    detail: `${paramName} in ${this.constructor.name}`
                })
            }
        }
    }

    // eslint-disable-next-line no-unused-vars
    async _calc_value(node, path, context, block_context) {
        throw new ExtException({message: 'Not implemented', detail: `${this.constructor.name}._calc_value`})
    }

    copyBlockContext(param = {}, block_context) {
        for (const [key, value] of Object.entries(block_context)) {
            if (!key.startsWith('_') && value !== undefined) {
                param[key] = value
            }
        }
    }
}

export class SimpleBlockNoStep extends SimpleBlock {
    step = false
}
