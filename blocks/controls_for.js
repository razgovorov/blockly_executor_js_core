import {SimpleBlockNoStep} from "@/BlocklyExecutor/Core/block_templates/simple_block"
import {ServiceException} from "@/BlocklyExecutor/Core/exceptions"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"
import ExtException from "@/Helpers/ExtException"

export default class ControlsFor extends SimpleBlockNoStep {
    required_param = []
    statement_inputs = ['DO']

    async _calc_value(node, path, context, block_context) {
        try {
            if (!objHasOwnProperty(block_context, 'INDEX')) {
                block_context['INDEX'] = block_context['FROM']
            }
            let node_loop = this.workspace.find_statement_by_name(node, this.statement_inputs[0])
            while (block_context['INDEX'] <= block_context['TO']) {
                this._check_step(context, block_context)
                this.set_variable(context, block_context['VAR'], block_context['INDEX'])
                await this.execute_all_next(node_loop, path, context, block_context, true)
                block_context['INDEX'] += block_context['BY']
                context.is_next_step = true
            }
        } catch (err) {
            if (err instanceof ServiceException) {
                throw err
            }
            switch (err.constructor.name) {
                case 'ExtException':
                    throw err
                default:
                    throw new ExtException({parent:err})
            }
        }
    }
}
