import {SimpleBlockNoStep} from "@/BlocklyExecutor/Core/block_templates/simple_block"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"

export default class ControlsForeach extends SimpleBlockNoStep {
    required_param = ['VAR', 'LIST']
    statement_inputs = ['DO']

    async _calc_value(node, path, context, block_context) {
        if (!objHasOwnProperty(block_context, '_INDEX')) {
            block_context['_INDEX'] = 0
        }
        if (block_context['LIST']) {
            let node_loop = this.workspace.find_statement_by_name(node, this.statement_inputs[0])
            while (block_context['_INDEX'] < block_context['LIST'].length){
                this._check_step(context, block_context)
                this.set_variable(context, block_context['VAR'], block_context['LIST'][block_context['_INDEX']])
                await this.execute_all_next(node_loop, path, context, block_context, true)
                block_context['_INDEX'] += 1
                context.is_next_step = true
            }
        }
    }
}
