import Block from "@/BlocklyExecutor/Core/Block"
import {ServiceException, ErrorInBlock} from "@/BlocklyExecutor/Core/exceptions"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"

export default class ControlsIf extends Block {
    async _execute(node, path, context, block_context) {
        try {
            let mutation_else_if = 'elseif'
            let mutation_else = 'else'
            // if (this.workspace === 'json') {
            //     mutation_else_if = 'elseif'
            //     mutation_else = 'else'
            // } else {
            //     mutation_else_if = 'elseIfCount'
            //     mutation_else = 'hasElse'
            // }

            let if_count = Number(this.workspace.find_mutation_by_name(node, mutation_else_if, 0)) + 1
            let defined_else = Number(this.workspace.find_mutation_by_name(node, mutation_else, 0))
            let if_complete = false
            let j = undefined
            if (if_count) {
                for (j = 0; j < if_count; j++) {
                    // рассчитываем все if
                    let _key = `IF${j}`
                    let complete = _key in block_context
                    let result
                    if (complete) {
                        result = block_context[_key]
                    } else {
                        let node_if = this.workspace.find_input_by_name(node, `IF${j}`)
                        if (!node_if) {
                            throw new Error(`Bad ${_key} ${path}`)
                        }
                        result = await this.execute_all_next(node_if, `${path}.if${j}`, context, block_context)
                        block_context[_key] = result
                    }
                    if (result) {
                        if_complete = true
                        break
                    }
                }
            }
            this._check_step(context, block_context)
            if (if_complete && j !== undefined) {
                if (!objHasOwnProperty(block_context, '_do')) {
                    let node_do = this.workspace.find_statement_by_name(node, `DO${j}`)
                    if (node_do === undefined) {
                        return
                    }
                    //  throw   new Error(`Bad if( DO {j} {path}')
                    await this.execute_all_next(node_do, `${path}.do${j}`, context, block_context, true)
                    block_context['_do'] = undefined
                }
            } else {
                if (defined_else) {
                    if (!objHasOwnProperty(block_context, '_do')) {
                        let node_do = this.workspace.find_statement_by_name(node, 'ELSE')
                        if (node_do === undefined) {
                            return
                        }
                        //  throw   new Error(`Bad else DO {path}')
                        await this.execute_all_next(node_do, `${path}.else`, context, block_context, true)
                        block_context['_do'] = undefined
                    }
                }
            }
        } catch (err) {
            if (err instanceof ServiceException) {
                throw err
            }
            switch (err.constructor.name) {
                case 'ExtException':
                    throw err
                default:
                    throw new ErrorInBlock({parent: err})
            }
        }
    }
}
