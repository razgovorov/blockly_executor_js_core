import {SimpleBlockNoStep} from "@/BlocklyExecutor/Core/block_templates/simple_block"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"
import {ErrorInBlock, ServiceException} from "@/BlocklyExecutor/Core/exceptions"


export default class ControlsRepeatExt extends SimpleBlockNoStep {
    statement_inputs = ['DO']

    async _calc_value(node, path, context, block_context) {
        try {
            if (!objHasOwnProperty(block_context, '_INDEX')) {
                block_context['_INDEX'] = 1
            }
            let node_loop = this.workspace.find_statement_by_name(node, this.statement_inputs[0])
            while (block_context['_INDEX'] <= Number(block_context['TIMES'])) {
                this._check_step(context, block_context)
                await this.execute_all_next(node_loop, path, context, block_context, true)
                block_context['_INDEX'] += 1
                context.is_next_step = true
            }
        } catch (err) {
            if (err instanceof ServiceException) {
                throw err
            }
            switch (err.constructor.name) {
                case 'ExtException':
                    throw err
                default:
                    throw new ErrorInBlock({parent: err})
            }
        }

    }
}
