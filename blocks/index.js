import controls_for from './controls_for'
import controls_foreach from './controls_foreach'
import controls_if from './controls_if'
import controls_repeat_ext from './controls_repeat_ext'
import lists_create_with from './lists_create_with'
import lists_get_index from './lists_get_index'
import lists_length from './lists_length'
import lists_set_index from './lists_set_index'
import logic_boolean from './logic_boolean'
import logic_compare from './logic_compare'
import logic_operation from './logic_operation'
import math_arithmetic from './math_arithmetic'
import math_number from './math_number'
import procedures_callnoreturn from './procedures_callnoreturn'
import procedures_callreturn from './procedures_callreturn'
import procedures_defreturn from './procedures_defreturn'
import procedures_defnoreturn from './procedures_defnoreturn'
import text from './text'
import logic_null from './logic_null'
import text_join from './text_join'
import variables_get from './variables_get'
import variables_set from './variables_set'
import logic_negate from './logic_negate'

export default {
    blocks: {
        controls_for,
        controls_foreach,
        controls_if,
        controls_repeat_ext,
        lists_create_with,
        lists_get_index,
        lists_length,
        lists_set_index,
        logic_boolean,
        logic_compare,
        logic_operation,
        math_arithmetic,
        math_number,
        procedures_callnoreturn,
        procedures_callreturn,
        procedures_defreturn,
        procedures_defnoreturn,
        text,
        text_join,
        variables_get,
        variables_set,
        logic_null,
        logic_negate
    }
}
