import Block from "@/BlocklyExecutor/Core/Block"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"

export default class ListsCreateWith extends Block {
    async _execute(node, path, context, block_context) {
        let total_size = this.workspace.find_mutation_by_name(node, 'items', '0')
        let result = []
        if (!total_size) {
            return result
        }
        if (!objHasOwnProperty(block_context, 'value')) {
            block_context['value'] = []
        }
        let current_size = block_context['value'].length
        if (current_size !== total_size) {
            for (let j = current_size; j < total_size; j++) {
                let node_value = this.workspace.find_input_by_name(node, `ADD${j}`)
                if (node_value === undefined) {
                    throw new Error(`плохой блок ADD${j}`)
                }
                result = await this.execute_all_next(node_value, `${path}.${j}`, context, block_context)
                block_context['value'].push(result)
            }
            this._check_step(context, block_context)
            return block_context['value']
        }
    }
}
