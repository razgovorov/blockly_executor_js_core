import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"


export default class ListsGetIndex extends SimpleBlock {

    async _calc_value(node, path, context, block_context) {
        let array = this.get_variable(context, block_context['VAR'])
        let mode = block_context['MODE']
        let where = block_context['WHERE']
        switch (mode) {
            case 'GET':
                switch (where) {
                    case 'FROM_START':
                        return array.slice(block_context['AT'], array.length)
                    case 'FROM_END':
                    case 'FIRST':
                    case 'LAST':
                    case 'RANDOM':
                    default:
                        throw new Error(`NotImplemented block ${this.constructor.name} where ${where} `)
                }
            case 'GET_REMOVE':
            case 'REMOVE':
            default:
                throw new Error(`NotImplemented block ${this.constructor.name} mode ${mode} `)
        }
    }
}
