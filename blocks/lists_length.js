import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"
import {getType} from "@/Helpers/BaseHelper"


export default class ListsLength extends SimpleBlock {

    async _calc_value(node, path, context, block_context) {
        let array = block_context['VALUE'] || []
        if (getType(array) === 'array') {
            return array.length
        }
        return 0
    }
}
