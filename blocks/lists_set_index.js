import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"


export default class ListsSetIndex extends SimpleBlock {

    async _calc_value(node, path, context, block_context) {
        let array = this.get_variable(context, block_context['VAR'])
        let mode = block_context['MODE']
        let where = block_context['WHERE']
        switch (mode) {
            case 'SET':
                switch (where) {
                    case 'FROM_START':
                        array[block_context['AT']] = block_context['TO']
                        return undefined
                    case 'FROM_END':
                        array[block_context['AT'] * -1] = block_context['TO']
                        return undefined
                    case 'FIRST':
                        throw new Error(`NotImplemented block ${this.constructor.name} where ${where}`)
                    case 'LAST':
                        array.push(block_context['TO'])
                        return undefined
                    case 'RANDOM':
                    default:
                        throw new Error(`NotImplemented block ${this.constructor.name} where ${where}`)
                }
            default:
                throw new Error(`NotImplemented block ${this.constructor.name} mode ${mode}`)
        }
    }
}
