import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"
import ExtException from "@/Helpers/ExtException"

export default class LogicCompare extends SimpleBlock {
    required_param = ['A', 'B', 'OP']
    operations = {
        EQ: _eq,
        NEQ: _neq,
        GT: _gt,
        GTE: _gte,
        LT: _lt,
        LTE: _lte,
    }

    async _calc_value(node, path, context, block_context) {
        let operation = block_context['OP']
        if (!objHasOwnProperty(this.operations, operation)) {
            throw new ExtException({detail: `${this.constructor.name}: Operation {operation} not supported`})
        }
        return this.operations[operation](block_context)
    }
}

function _eq(block_context) {
    return block_context['A'] === block_context['B']
}

function _neq(block_context) {
    return block_context['A'] !== block_context['B']
}

function _gt(block_context) {
    return block_context['A'] > block_context['B']
}

function _gte(block_context) {
    return block_context['A'] >= block_context['B']
}

function _lt(block_context) {
    return block_context['A'] < block_context['B']
}

function _lte(block_context) {
    return block_context['A'] <= block_context['B']
}
