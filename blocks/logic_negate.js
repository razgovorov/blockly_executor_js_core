import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class LogicNegate extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        return !block_context.BOOL
    }
}
