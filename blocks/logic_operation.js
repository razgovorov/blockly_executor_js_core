import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";
import {ErrorInBlock} from "@/BlocklyExecutor/Core/exceptions";

export default class LogicOperation extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        switch (block_context['OP']) {
            case 'AND':
                return block_context['A'] && block_context['B']
            case'OR':
                return block_context['A'] || block_context['B']
            default:
                throw new ErrorInBlock({detail: `${this.constructor.name}: Operation ${block_context["OP"]} not supported`})
        }
    }
}
