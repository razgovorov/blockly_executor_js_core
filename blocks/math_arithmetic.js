import {UserError} from "@/Helpers/ExtException"
import {getType} from "@/Helpers/BaseHelper"
import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";


export default class MathArithmetic extends SimpleBlock{
    required_param = ['OP', 'A', 'B']

    async _calc_value(node, path, context, block_context){
        if( !getType(block_context['A']) !== 'number') {
            throw new UserError({
                message: 'В арифметическую операцию передано не числовое значение',
                detail: String(block_context['A']),
                dump: {'block_id': this.block_id}
            })
        }
        if( !getType(block_context['B']) !== 'number') {
            throw new UserError({
                message: 'В арифметическую операцию передано не числовое значение',
                detail: String(block_context['A']),
                dump: {'block_id': this.block_id}
            })
        }
        return operations[block_context['OP']](block_context['A'], block_context['B'])
    }
}

function add(a, b) {
    return a + b
}

function minus(a, b) {
    return a - b
}

function multiply(a, b) {
    return a * b
}

function divide(a, b) {
    return a / b
}

function power(a, b) {
    return a ^ b
}

const operations = {
    'ADD': add,
    'MINUS': minus,
    'MULTIPLY': multiply,
    'DIVIDE': divide,
    "POWER": power
}
