import {SimpleBlockNoStep} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class MathNumber extends SimpleBlockNoStep {
    required_param = ['NUM']

    async _calc_value(node, path, context, block_context) {
        return Number(block_context['NUM'])
    }
}
