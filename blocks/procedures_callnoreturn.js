import Block from "@/BlocklyExecutor/Core/Block"
import {ReturnFromFunction} from "@/BlocklyExecutor/Core/exceptions"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"
import {UserError} from "@/Helpers/ExtException"

export default class ProceduresCallnoreturn extends Block {
    async _execute2(node, path, context, block_context) {
        try {
            if (!objHasOwnProperty(block_context, this._result)) {
                let endpoint = this.workspace.find_mutation_by_name(node, 'name')
                if (!objHasOwnProperty(block_context, '__params')) {
                    block_context['__params'] = await this.calc_param_value(node, path, context, block_context)
                }
                let handler = this.workspace.init_procedure_block(this.executor, endpoint)
                this._check_step(context, block_context)

                context.variable_scope_add(block_context, block_context['__params'])
                block_context[this._result] = await handler.execute(handler.node, path, context, block_context)
                context.variable_scope_remove(block_context)
                return block_context[this._result]
            }
        } catch (err) {
            // if (err instanceof ServiceException) {
            //     switch (err.constructor.name) {
            //         case 'ReturnFromFunction':
            //             context.set_next_step(this.block_id)
            //             context.clear_child_context(block_context)
            //             return err.args[0]
            //         default:
            //             throw err
            //     }
            // }
            // switch (err.constructor.name) {
            //     case 'ExtException':
            //         throw err
            //     default:
            //         throw new ExtException({parent: err})
            // }
            if (err instanceof ReturnFromFunction) {
                context.set_next_step(this.block_id)
                context.clear_child_context(block_context)
                return err.args[0]
            }
            if (err instanceof UserError) {
                block_context[this._result] = undefined
                context.variable_scope_remove(block_context)
                throw err
            }
        }
    }

    async calc_param_value(node, path, context, block_context) {
        let args = this.workspace.find_mutation_args(node)
        let inputs = this.workspace.find_inputs(node)
        let function_params = {}
        if (inputs) {
            for (let i = 0; i <= args.length; i++) {
                let arg_name = args[i]
                if (!objHasOwnProperty(block_context, arg_name)) {
                    let arg_node = inputs[`ARG${i}`]
                    let _value = undefined
                    if (arg_node) {
                        _value = await this.execute_all_next(arg_node, `${path}.${i}`, context, block_context)
                    }
                    block_context[arg_name] = _value
                    function_params[arg_name] = _value
                }
            }
        }
        return function_params
    }

    async _execute(node, path, context, block_context) {
        await this._execute2(node, path, context, block_context)
        return this.workspace.find_next_statement(node)
    }
}
