import ProceduresCallnoreturn from "@/BlocklyExecutor/Core/blocks/procedures_callnoreturn";

export default class ProceduresCallreturn extends ProceduresCallnoreturn {
    async _execute(node, path, context, block_context) {
        return await this._execute2(node, path, context, block_context)
    }
}
