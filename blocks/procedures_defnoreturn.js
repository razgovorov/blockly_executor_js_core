import Block from "@/BlocklyExecutor/Core/Block"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"
import {ServiceException} from "@/BlocklyExecutor/Core/exceptions"
import ExtException from "@/Helpers/ExtException"

export default class ProceduresDefnoreturn extends Block {
    async _execute(node, path, context, block_context) {
        this._check_step(context, block_context)
        let code = this.workspace.find_statement_by_name(node, 'STACK')
        let name = this.workspace.find_field_by_name(node, 'NAME')
        if (code && !objHasOwnProperty(block_context, '_stack')) {
            try {
                await this.execute_all_next(code, `${path}.${name}`, context, block_context, true)
            } catch (err) {
                if (err instanceof ServiceException) {
                    throw err
                }
                switch (err.constructor.name) {
                    case 'ExtException':
                        throw err
                    default:
                        throw new ExtException({parent: err})
                }
            }
        }
    }
}
