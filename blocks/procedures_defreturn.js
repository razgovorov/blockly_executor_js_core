import ProceduresDefnoreturn from "@/BlocklyExecutor/Core/blocks/procedures_defnoreturn";

export default class ProceduresDefreturn extends ProceduresDefnoreturn {
    async _execute(node, path, context, block_context) {
        await super._execute(node, path, context, block_context)
        let _return_node = this.workspace.find_input_by_name(node, 'RETURN')
        let name = this.workspace.find_field_by_name(node, 'NAME')
        context.set_next_step(this.block_id)
        return await this.execute_all_next(_return_node, `${path}.${name}`, context, block_context)
    }
}
