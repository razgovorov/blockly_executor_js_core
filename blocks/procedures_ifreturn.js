import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"
import {ReturnFromFunction} from "@/BlocklyExecutor/Core/exceptions";

export default class ProceduresIfreturn extends SimpleBlock {
    async _calc_value(node, path, context, block_context){
        if( block_context['CONDITION']) {
             throw new ReturnFromFunction(block_context['VALUE'])
        }
    }
}
