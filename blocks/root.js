import {objHasOwnProperty} from "@/Helpers/BaseHelper"
import ExtException from "@/Helpers/ExtException"
import {Block} from "blockly";
import {ServiceException} from "@/BlocklyExecutor/Core/exceptions";

export default class Root extends Block {

    async _execute(node, path, context, block_context) {
        try {
            let standard_blocks = this.workspace.find_child_blocks(node)
            if (!objHasOwnProperty(block_context, this._result)) {
                block_context[this._result] = {}
            }
            for (let i = 0; i < standard_blocks.length; ++i) {
                let block_node = standard_blocks[i]
                let block_id = block_node['id']
                let block_type = block_node['type']
                if (block_type in ['procedures_defreturn', 'procedures_defnoreturn']) {
                    continue
                }
                if (!objHasOwnProperty(block_context[this._result], block_id)) {
                    block_context[this._result][block_id] = {}
                }
                if (!objHasOwnProperty(block_context[this._result][block_id], this._result)) {
                    await this.execute_child_block(
                        block_node, path, context, block_context[this._result][block_id])
                    block_context[this._result][block_id][this._result] = true
                }
                if (!objHasOwnProperty(block_context[this._result][block_id], '_next')) {
                    let next_node = this.workspace.find_next_statement(block_node)
                    if (next_node) {
                        await this.execute_all_next(next_node, path, context, block_context[this._result][block_id])
                    }
                    block_context[this._result][block_id]['_next'] = true
                    context.set_next_step()
                }
            }
        } catch (err) {
            if (err instanceof ServiceException) {
                throw err
            }
            switch (err.constructor.name) {
                case 'ExtException':
                    throw err
                default:
                    throw new ExtException({parent: err})
            }
        }
    }

    async _before_execute(node, path, context, block_context){
        this.block_id = 'root'
        this.block_type = 'root'
        block_context['__id'] = this.block_id
        block_context['__path'] = `${path}.${this.block_type}`
    }
}
