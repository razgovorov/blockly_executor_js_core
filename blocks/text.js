import Block from "@/BlocklyExecutor/Core/Block"


export default class Text extends Block {
    // eslint-disable-next-line no-unused-vars
    async _execute(node, path, context, block_context) {
        return this.workspace.find_field_by_name(node, 'TEXT')
    }
}
