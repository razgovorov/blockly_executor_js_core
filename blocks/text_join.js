import {objHasOwnProperty} from "@/Helpers/BaseHelper"
import {Block} from "blockly";

export default class TextJoin extends Block {
    async _execute(node, path, context, block_context) {
        let text_count = Number(this.workspace.find_mutation_by_name(node, 'items', 0))
        let result = ""
        for (let j = 0; j <= text_count; j++) {
            let _key = `ADD${j}`
            let complete = objHasOwnProperty(block_context, _key)
            if (complete) {
                result = block_context[_key]
            } else {
                let node_text = this.workspace.find_input_by_name(node, `ADD${j}`)
                let next_node_result = await this.execute_all_next(node_text, `{path}.add${j}`, context, block_context)
                result += String(next_node_result)
            }
        }
        return result
    }
}
