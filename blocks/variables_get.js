import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class VariablesGet extends SimpleBlock {
    required_param = ['VAR']

    async _calc_value(node, path, context, block_context) {
        return this.get_variable(context, block_context['VAR'])
    }
}
