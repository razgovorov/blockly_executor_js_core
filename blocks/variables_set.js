import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class VariablesSet extends SimpleBlock {
    required_param = ['VAR']

    async _calc_value(node, path, context, block_context) {
        this.set_variable(context, block_context['VAR'], block_context['VALUE'])
    }
}
