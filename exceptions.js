import ExtException from "@/Helpers/ExtException"

export class ServiceException extends Error {}

export class LimitCommand extends ServiceException {}

export class WorkspaceNotFound extends ServiceException {}

export class ErrorInBlock extends ExtException {}

export class DeferredOperation extends ServiceException {
    constructor(commandUuid, context, block_context) {
        super()
        this.commandUuid = commandUuid
        this.context = context
        this.block_context = block_context
    }

    get_context() {
        return this.block_context
    }

    toCommand() {
        return [{
            'method': this.commandUuid,
            'params': this.context,
            'uuid': this.block_context['__deferred']
        }]
    }
}

export class StepForward extends ServiceException {
    constructor(args) {
        super()
        this.args = args
    }

    get_context() {
        return this.args[2].to_dict()
    }

    to_command() {
        return [this.args[0], this.args[1]]
    }
}

export class ReturnFromFunction extends ServiceException {
    constructor(...args) {
        super();
        this.args = args
    }
}
